import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import React from 'react';
import { StyledFirebaseAuth } from 'react-firebaseui';


Login.propTypes = {

};

function Login(props) {
    // Configure FirebaseUI.
    const uiConfig = {
        // Popup signin flow rather than redirect flow.
        signInFlow: 'redirect',
        // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
        signInSuccessUrl: '/todos',
        // We will display Google and Facebook as auth providers.
        signInOptions: [
            firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        ],
    };

    return (
        <div>
            <div className='text-center' style={{ textAlign: 'center' }}>
                <h2>Login Form</h2>

                <p>or login with social accounts</p>
            </div>
            <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
        </div>
    );
}

export default Login;