import React from 'react';
import { Link } from 'react-router-dom';

Home.propTypes = {

};

function Home(props) {
    return (
        <>
            <Link to="/todos">Todo List</Link>
        </>
    );
}

export default Home;