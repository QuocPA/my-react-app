import { ArrowBack } from '@mui/icons-material';
import { Box, Button, Container, TextField, Typography } from '@mui/material';
import todoApi from 'api/todoApi';
import { useFormik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import * as Yup from 'yup';
import '../todo.scss';
import { addTodo, updateTodo } from '../todoSlice';

TodoAdd.propTypes = {

};

function TodoAdd(props) {
    const { todoId } = useParams();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [formValues, setFormValues] = useState({
        title: '',
        description: ''
    });

    const isAddTodo = !todoId;

    const formik = useFormik({
        initialValues: formValues,
        validationSchema: Yup.object({
            title: Yup.string().required('Title is required'),
            description: Yup.string().required('Description is required')
        }),
        onSubmit: async (values) => {
            if (isAddTodo) {
                // add todo
                await dispatch(addTodo(values));
            } else {
                // update todo
                await dispatch(updateTodo(values));
            }
            navigate("/todos");
        },
        enableReinitialize: true
    })

    useEffect(() => {
        if (!isAddTodo) {
            todoApi.fetchTodoId(todoId).then((res) => {
                setFormValues({ ...formValues, title: res.data.title, description: res.data.description, id: res.data.id })
            })
        }
    }, [])

    return (
        <>
            <Button onClick={() => { navigate(-1) }}><ArrowBack fontSize="large"></ArrowBack></Button>
            <Box
                component="main"
                sx={{
                    alignItems: 'center',
                    minHeight: '100%'
                }}
            >
                <Container maxWidth="sm">
                    <form onSubmit={formik.handleSubmit} onReset={formik.handleReset}>
                        <Box sx={{ my: 3 }}>
                            <Typography
                                color="textPrimary"
                                variant="h4"
                            >
                                Create a new todo
                            </Typography>
                        </Box>
                        <TextField
                            error={Boolean(formik.touched.title && formik.errors.title)}
                            fullWidth
                            helperText={formik.touched.title && formik.errors.title}
                            label="Title"
                            name="title"
                            onChange={formik.handleChange}
                            value={formik.values.title}
                            variant="outlined"
                            style={{ marginBottom: "20px" }}
                        />

                        <TextField
                            error={Boolean(formik.touched.description && formik.errors.description)}
                            fullWidth
                            helperText={formik.touched.description && formik.errors.description}
                            label="Description"
                            name="description"
                            onChange={formik.handleChange}
                            value={formik.values.description}
                            variant="outlined"
                            style={{ marginBottom: "20px" }}
                        />

                        <Button color="primary" fullWidth size="large" type="submit" variant="contained">{isAddTodo ? "Add" : "Update"}</Button>
                    </form>
                </Container>
            </Box>
        </>
    );
}

export default TodoAdd;