import { Delete, Edit } from '@mui/icons-material';
import {
    Box, Button, CircularProgress, Container, IconButton, Paper, Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow, Typography
} from '@mui/material';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { deleteTodo, fetchTodos } from '../todoSlice';

function TodoList() {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const todos = useSelector(state => state.todos.todos)
    const loading = useSelector(state => state.todos.loading)


    function handleEdit(id) {
        navigate(`/todos/${id}`)
    }

    function handleDelete(id) {
        dispatch(deleteTodo(id));
    }

    useEffect(() => {
        const fetchDataTodos = () => {
            dispatch(fetchTodos());
        }

        fetchDataTodos();
    }, []);

    return (
        <div>
            <Box
                component="main"
                sx={{
                    alignItems: 'center',
                    minHeight: '100%'
                }}
            >
                <Container
                    maxWidth="lg"
                >
                    <Box
                        sx={{
                            my: 3
                        }}
                    >
                        <Typography color="textPrimary" variant="h4" align="center">TODO LIST</Typography>
                    </Box>

                    <Button onClick={() => { navigate('todoAdd') }} variant="contained">Add Todo</Button>

                    <TableContainer component={Paper}>
                        {
                            loading && (
                                <Box
                                    sx={{
                                        display: "flex",
                                        alignItems: "center",
                                        justifyContent: "center",
                                        minHeight: "400px",
                                    }}
                                >
                                    <CircularProgress />
                                </Box>
                            )
                        }
                        {
                            !loading && (
                                <>
                                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell width="100" align="center">Title</TableCell>
                                                <TableCell width="100" align="center">Description</TableCell>
                                                <TableCell width="100" align="center">Action</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {todos.map((todo) => (
                                                <TableRow
                                                    key={todo.id}
                                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                >
                                                    <TableCell align="center">{todo.title}</TableCell>
                                                    <TableCell align="center">{todo.description}</TableCell>
                                                    <TableCell align="center">
                                                        <IconButton onClick={() => handleDelete(todo.id)} aria-label="delete" size="small">
                                                            <Delete fontSize="inherit" />
                                                        </IconButton>
                                                        <IconButton onClick={() => handleEdit(todo.id)} aria-label="delete" size="small">
                                                            <Edit fontSize="inherit" />
                                                        </IconButton>
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </>
                            )
                        }
                    </TableContainer>
                </Container>
            </Box>
        </div>
    );
}

export default TodoList;