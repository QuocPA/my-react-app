import React from 'react';
import { Routes, Route } from 'react-router-dom';
import NotFound from '../../components/NotFound';
import TodoAdd from './pages/TodoAdd';
import TodoList from './pages/TodoList';

Todo.propTypes = {

};

function Todo(props) {
    return (
        <Routes>
            <Route path="/" element={<TodoList />} />
            <Route path="todoAdd" element={<TodoAdd />} />
            <Route path=":todoId" element={<TodoAdd />} />

            <Route path="todos/*" element={<NotFound />} />
        </Routes>
    );
}

export default Todo;