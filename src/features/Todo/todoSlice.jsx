import { createAsyncThunk, createReducer } from "@reduxjs/toolkit";
import todoApi from "api/todoApi";

export const fetchTodos = createAsyncThunk('todos/fetchTodos', async () => {
    const response = await todoApi.fetchTodos();
    return response;
})

export const deleteTodo = createAsyncThunk('todos/deleteTodo', async (todoId, thunkAPI) => {
    await todoApi.deleteTodo(todoId);
    thunkAPI.dispatch(fetchTodos())
})

export const updateTodo = createAsyncThunk('todos/updateTodo', async (params) => {
    await todoApi.updateTodo(params);
})

export const addTodo = createAsyncThunk('todos/addTodo', async (params) => {
    await todoApi.addTodo(params);
})

export const fetchTodoId = createAsyncThunk('todos/fetchTodoId', async (todoId, thunkAPI) => {
    const response = await todoApi.fetchTodoId(todoId);
    return response.data;
})

const initialState = {
    todos: [],
    error: {},
    loading: true
}

function isPendingAction(action) {
    return action.type.endsWith('pending');
}

const todoReducer = createReducer(initialState, (builder) => {
    builder
        .addCase(fetchTodos.fulfilled, (state, action) => {
            state.loading = false;
            state.todos = action.payload.data;
        })
    builder
        .addCase(fetchTodos.rejected, (state, action) => {
            state.loading = false;
            state.error = action.payload;
        })
    builder
        .addMatcher(isPendingAction, (state, action) => {
            state.loading = true;
        })
})

export default todoReducer;