import { Container, Link } from '@mui/material';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import './style.scss';

Header.propTypes = {

};

function Header() {
    const navigate = useNavigate();
    function handleEasyFrontend(params) {
        // TODO: open link easy frontend
    }

    return (
        <Container fixed>
            <div className="header">
                <Link component="button" variant="body2" onClick={() => handleEasyFrontend}>Easy Frontend</Link>
                <Link component="button" variant="body2" onClick={() => { navigate("login") }}>Login</Link>
            </div>
        </Container>
    );
}

export default Header;