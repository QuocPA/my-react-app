import Login from "features/Auth";
import React, { Suspense, useEffect } from "react";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import Header from "./components/Header";
import NotFound from "./components/NotFound";
import Home from "./features/Home/home";
import Todo from "./features/Todo/todo";

import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';

function App() {
  // Configure Firebase.
  const config = {
    apiKey: 'AIzaSyACJCVwmDvYXCcYFj8KBV393wM4VJTox1w',
    authDomain: 'todowebapp-5f8b1.firebaseapp.com',
    // ...
  };
  firebase.initializeApp(config);

  useEffect(() => {
    const unregisterAuthObserver = firebase.auth().onAuthStateChanged(async (user) => {
      if (!user) {
        return;
      }
      const userToken = await user.getIdToken();
    });
    return () => unregisterAuthObserver();
  }, [])

  return (
    <Suspense fallback={<div>Loading ...</div>}>
      <BrowserRouter>
        <Header />

        <Routes>
          <Route index path="/" element={<Home />} />
          <Route path="login" element={<Login />} />
          <Route path="todos/*" element={<Todo />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </Suspense>
  );
}

export default App;
