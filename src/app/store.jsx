import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import todoReducer from 'features/Todo/todoSlice';

const middleware = [...getDefaultMiddleware({ thunk: true })];
const rootReducer = {
  todos: todoReducer,
}

const store = configureStore({
  reducer: rootReducer,
  middleware: middleware,
})


export default store;