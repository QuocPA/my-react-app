import axios from 'axios';
import queryString from 'query-string';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';

const getFirebaseToken = async () => {
    const currentUser = firebase.auth().currentUser;
    if (currentUser) return currentUser.getIdToken()

    // not logged in
    const hasRememberedAccount = localStorage.getItem('firebaseToken');
    if (!hasRememberedAccount) return null;

    //logged but current user not fetched
    return new Promise((resolve, reject) => {
        const waiting = setTimeout(() => {
            reject(null)
        }, 10000)

        const unregisterAuthObserver = firebase.auth().onAuthStateChanged(async (user) => {
            if (!user) { reject(null); }

            const userToken = await user.getIdToken();
            resolve(userToken)
            unregisterAuthObserver();
            clearTimeout(waiting)
        })
    })
}

const axiosClient = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    headers: {
        'content-type': 'application/json'
    },
    paramsSerializer: params => queryString.stringify(params)
})

axiosClient.interceptors.request.use(async (config) => {
    const token = await getFirebaseToken();
    if (token) {
        config.headers.Authorization = `Bearer ${token}`
    }
    return config;
})

axiosClient.interceptors.response.use((response) => {
    if (response && response.data) {
        return response.data;
    }

    return response;
}, (error) => {
    throw error;
})

export default axiosClient;