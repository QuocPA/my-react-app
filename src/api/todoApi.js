const { default: axiosClient } = require("./axiosClient");

const todoApi = {
    fetchTodos: () => {
        const url = 'todos';
        return axiosClient.get(url);
    },

    fetchTodoId: (todoId) => {
        const url = `todos/${todoId}`;
        return axiosClient.get(url);
    },

    deleteTodo: (todoId) => {
        const url = `todos/${todoId}`;
        return axiosClient.delete(url);
    },

    updateTodo: (params) => {
        const url = `todos/${params.id}`;
        return axiosClient.patch(url, params);
    },

    addTodo: (params) => {
        const url = 'todos';
        return axiosClient.post(url, params);
    }
}

export default todoApi;